import { Component } from '@angular/core';
import {NgbActiveModal, NgbDateStruct, NgbModal} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'jskaa';
  a;
  b = 'Your Name is:';
  age;
  model: NgbDateStruct;
  todayDate;
  c = 'Your Age is:'
  constructor(private modalService: NgbModal) {}
  open(content) {
    return this.modalService.open(content);
  }
  save(xyz) {
    this.a = this.b + xyz;
    this.todayDate = new Date();
    this.age =  this.todayDate.getFullYear() - this.model.year;

  }

}
